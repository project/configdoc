Introduction:
-------------
There is currently no easy way to download and share the complete manifests of a Drupal installation:
  * Which version is installed?
  * Which modules are installed?
  * How is Apache configured?
  * Is Apache even the web server that is running?
  * Which database is being used?
  * Which PHP version is being used?

As the number of poorly-documented mysterious bug reports accrue on drupal.org, we offer a way to let users document in which ecosystem that bug is happening.

Installation:
-------------
Normal module installation applies. The module is listed under the "Development" group.

Running:
--------
Go to Administer >> Reports >> Configuration State to view (and download) the contents of your current configuration.
