<?php

/**
 * @file
 * Document the current Drupal ecosystem .
 */

function configdoc_menu() {
  $items = array();
  $items['admin/reports/configdoc'] = array(
    'title' => 'Configuration State',
    'description' => 'Display configuration information for debugging',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('configdoc_page'),
    'access arguments' => array('access administration pages'),
    'weight' => 10,
    );
  return $items;
}

function configdoc_settings($form_state) {
  $form = array();
  
  $form['configdoc_show'] = array(
    '#type' => 'radios',
    '#options' => array('enabled' => t('Enabled only'), 'disabled' => t('Both enabled and disabled')),
    '#title' => t('Which modules to show'),
    '#default_value' => variable_get('configdoc_show', 'disabled'),
    );
  
  return system_settings_form($form);
}

function configdoc_page($form_state) {
  
  $configdoc_output = configdoc_create();
  $form = array();
  
  $form['body'] = array(
    '#type' => 'textarea',
    '#title' => t('Report'),
    '#default_value' => $configdoc_output,
    '#rows' => 30,
    '#cols' => 80,
    );
  
  $form['save'] = array(
    '#type' => 'submit',
    '#submit' => array('configdoc_save'),
    '#value' => t('Save as'),
    );
  
  return $form;
}

function configdoc_save($form, &$form_state) {
  
  $report = $form_state['values']['body'];
  header('Content-Type: text/txt');
  header('Content-Disposition: attachment; filename="configdoc.txt"');
  echo $report;
  exit();
  
}

function configdoc_create( ) {
  global $db_type;
  $output = array();
  
  $show = variable_get('configdoc_show', 'disabled');
  
  update_refresh();
  if ($available=update_get_available(TRUE)) {
    $files = module_rebuild_cache();
    module_load_include('inc', 'update', 'update.compare');
    foreach ($files as $module) {
      if (!isset($module->info['package']) || !$module->info['package']) {
        $module->info['package'] = t('Other');
      }
      if (substr($module->info['package'], 0, 5) == 'Core ') {
        // Sort core stuff first.
        $module->info['package'] = ' '. $module->info['package'];
      }
      $packages[$module->info['package']][$module->name] = (array) $module;
    }
    
    unset($files);
    ksort($packages);
    
    $data = update_calculate_project_data($available);
    
    $output[] = 'DRUPAL CORE';
    
    $output[] = ' Version: '. VERSION;
    $output[] = ' Recommended version: '. $data['drupal']['recommended'];
    $output[] = '';
    $output[] = 'PHP VERSION: '. phpversion();
    
    $output[] = '';
    $output[] = 'DATABASE TYPE: '. $db_type .'; VERSION: '. db_version();
    $output[] = '';
    $output[] = 'DRUPAL MODULES';
    $i = 0;
    
    foreach ($packages as $p_key => $modules) {
      $output[] = '  Package: '. $p_key;
      foreach ($modules as $key => $module) {
        if ($show == 'enabled' && !$module['status']) {
          continue;
        }
        $extra = array();
        if (isset($module['info']['version'])) {
          $stuff = $module['info']['version'];
          if (isset($data[$key]) && $data[$key]['recommended'] != $module['info']['version']) {
            $stuff .= '; Recommended version: '. $data[$key]['recommended'];
          }
          $extra[] = $stuff;
        }
        else {
          $extra[] = 'Version: unknown';
        }
        if ($show == 'disabled') {
          $extra[] = 'Status: '. ($module['status'] ? 'enabled' : 'disabled');
        }
        if (isset($module['info']['php']) && $module['info']['php'] != DRUPAL_MINIMUM_PHP) {
          $extra[] = 'Minimum PHP version required: '. $module['info']['php'];
        }
        $output[] = '    Module: '. $module['info']['name']
        . ($extra ? ('  ('. implode('; ', $extra) .')') : NULL);
      }
    }
    unset($data);
  }
  
  $output[] = '';
  $output[] = 'WEB SERVER: '. $_SERVER['SERVER_SOFTWARE'];
  $output[] = '';
  $output[] = 'DRUPAL MINIMUM SUPPORTED PHP VERSION: '. DRUPAL_MINIMUM_PHP;
  $output[] = '';
  $exts = get_loaded_extensions();
  if ($exts) {
    $output[] = '';
    $output[] = "PHP EXTENSIONS";
    foreach ($exts as $ext) {
      $output[] = '  '. $ext;
    }
  }
  if (stripos($_SERVER['SERVER_SOFTWARE'], 'apache') !== FALSE && in_array('apache2handler', $exts)) {
    $mods = apache_get_modules();
    $output[] = '';
    $output[] = 'APACHE MODULES';
    foreach ($mods as $mod)
      $output[] = '  '. $mod;
  }
  
  $output[] = '';
  $output[] = 'USER BROWSER: '. $_SERVER['HTTP_USER_AGENT'];
  $output[] = '';
  $output[] = 'CLEAN URLS: '. (variable_get('clean_url', 0) ? "enabled" : "disabled");
  $output[] = '';
  
  $output[] = t('CRON LAST RUN: @time ago', array('@time' => format_interval(time() - variable_get('cron_last', 0))));
  //$output[] = "";
  //$output[] = t('Last checked: @time ago', array('@time' => format_interval(time() - variable_get('update_last_check', 0))));
  
  return count($output) ? implode("\r\n", $output) : 'Report error';
}
